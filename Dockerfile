FROM ubuntu:18.04

RUN apt-get update && apt-get install -y wget

RUN wget --directory-prefix=/tmp https://bitbucket.org/derek_w/stm8l-ir-tx/downloads/sdcc-3.9.0-amd64-unknown-linux2.5.tar.bz2

RUN tar xf /tmp/sdcc-3.9.0-amd64-unknown-linux2.5.tar.bz2 --directory=/usr/local/ --strip=1 --exclude=INSTALL.txt --exclude=README.txt

COPY main.c /src

WORKDIR /src

CMD ["/usr/bin/sdcc", "-mstm8", "--std-c99", "/src/main.c"]

