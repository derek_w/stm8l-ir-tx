#include <stdint.h>

#define POWER                  0x8A
#define SPOT                   0x84
#define CLEAN                  0x88
#define MAX                    0x85
#define LEFT                   0x81
#define LEFT_FORWARD           0x8B
#define FORWARD                0x82
#define RIGHT_FORWARD          0x8C
#define RIGHT                  0x83
#define PAUSE                  0x89
#define VIRTUALWALL_LIGHTHOUSE 0xA2

#define STARTUP_DELAY          5
#define PULSE_COUNT            3

#define WFE_CR1         (*(volatile uint8_t *)0x50A6)
#define WFE_CR2         (*(volatile uint8_t *)0x50A7)

#define RST_CR          (*(volatile uint8_t *)0x50B0)
#define RST_SR          (*(volatile uint8_t *)0x50B1)

#define CLK_CKDIVR      (*(volatile uint8_t *)0x50C0)
#define CLK_PCKENR      (*(volatile uint8_t *)0x50C3)
#define CLK_EN_TIM2     (1 << 0)
#define CLK_EN_TIM3     (1 << 1)
#define CLK_EN_TIM4     (1 << 2)
#define CLK_EN_I2C      (1 << 3)
#define CLK_EN_SPI      (1 << 4)
#define CLK_EN_USART    (1 << 5)
#define CLK_EN_AWU_BEEP (1 << 6)

#define PORTA_ODR       (*(volatile uint8_t *)0x5000)
#define PORTA_IDR       (*(volatile uint8_t *)0x5001)
#define PORTA_DDR       (*(volatile uint8_t *)0x5002)
#define PORTA_CR1       (*(volatile uint8_t *)0x5003)
#define PORTA_CR2       (*(volatile uint8_t *)0x5004)

#define PORTB_ODR       (*(volatile uint8_t *)0x5005)
#define PORTB_IDR       (*(volatile uint8_t *)0x5006)
#define PORTB_DDR       (*(volatile uint8_t *)0x5007)
#define PORTB_CR1       (*(volatile uint8_t *)0x5008)
#define PORTB_CR2       (*(volatile uint8_t *)0x5009)

#define PORTx_VAL(PIN)        (1 << PIN)

#define PORTx_OUTPUT(PIN)     (1 << PIN)
#define PORTx_PULLUP(PIN)     (1 << PIN)
#define PORTx_PUSH_PULL(PIN)  (1 << PIN)
#define PORTx_INT_EN(PIN)     (1 << PIN)
#define PORTx_HS_OUT(PIN)     (1 << PIN)

#define AWU_CSR         (*(volatile uint8_t *)0x50F0)
#define AWU_APR         (*(volatile uint8_t *)0x50F1)
#define AWU_TBR         (*(volatile uint8_t *)0x50F2)

#define TIM2_CR1        (*(volatile uint8_t *)0x5250)
#define TIM2_CCMR1      (*(volatile uint8_t *)0x5258)
#define TIM2_CCER1      (*(volatile uint8_t *)0x525A)
#define TIM2_CNTRH      (*(volatile uint8_t *)0x525B)
#define TIM2_CNTRL      (*(volatile uint8_t *)0x525C)
#define TIM2_PSCR       (*(volatile uint8_t *)0x525D)
#define TIM2_ARRH       (*(volatile uint8_t *)0x525E)
#define TIM2_ARRL       (*(volatile uint8_t *)0x525F)
#define TIM2_CCR1H      (*(volatile uint8_t *)0x5260)
#define TIM2_CCR1L      (*(volatile uint8_t *)0x5261)
#define TIM2_BKR        (*(volatile uint8_t *)0x5264)
#define TIM2_OISR       (*(volatile uint8_t *)0x5265)

#define TIM3_CR1        (*(volatile uint8_t *)0x5280)
#define TIM3_CCMR1      (*(volatile uint8_t *)0x5288)
#define TIM3_CCER1      (*(volatile uint8_t *)0x528A)
#define TIM3_CNTRH      (*(volatile uint8_t *)0x528B)
#define TIM3_CNTRL      (*(volatile uint8_t *)0x528C)
#define TIM3_PSCR       (*(volatile uint8_t *)0x528D)
#define TIM3_ARRH       (*(volatile uint8_t *)0x528E)
#define TIM3_ARRL       (*(volatile uint8_t *)0x528F)
#define TIM3_CCR1H      (*(volatile uint8_t *)0x528F)
#define TIM3_CCR1L      (*(volatile uint8_t *)0x528F)
#define TIM3_BKR        (*(volatile uint8_t *)0x5294)
#define TIM3_OISR       (*(volatile uint8_t *)0x5295)

#define TIM4_CR1        (*(volatile uint8_t *)0x52E0)
#define TIM4_IER        (*(volatile uint8_t *)0x52E3)
#define TIM4_SR1        (*(volatile uint8_t *)0x52E4)
#define TIM4_CNTR       (*(volatile uint8_t *)0x52E6)
#define TIM4_PSCR       (*(volatile uint8_t *)0x52E7)
#define TIM4_ARR        (*(volatile uint8_t *)0x52E8)

#define TIMx_CR1_CEN    (1 << 0)
#define TIMx_CR1_UDIS   (1 << 1)
#define TIMx_CR1_URS    (1 << 2)
#define TIMx_CR1_OPM    (1 << 3)
#define TIMx_CR1_DIR    (1 << 4)
#define TIMx_CR1_ARPE   (1 << 7)

#define TIMx_CCMR1_FORCE_INACTIVE 0x40
#define TIMx_CCMR1_FORCE_ACTIVE 0x50

#define IR_CR              (*(volatile uint8_t *)0x52FF)
#define IR_CR_IR_EN        (1 << 0)
#define IR_CR_HS_EN        (1 << 1)

#define CFG_GCR            (*(volatile uint8_t *)0x7F60)
#define CFG_GCR_SWIM_DIS   (1 << 0)

#define IR_PIN  0    // PortA
#define LED_PIN 3    // PortB
#define SW1_PIN 7    // PortB

volatile uint32_t tickCount = 0;

void delayTick( uint32_t ticks )
{
   uint32_t tickStart = tickCount;

   while(( tickCount - tickStart ) < ticks )
   {
      __asm__("NOP");
   }
}

void sleepTick( uint32_t ticks )
{
   uint32_t tickStart = tickCount;

   while(( tickCount - tickStart ) < ticks )
   {
      __asm__("WFI");
   }
}

void TIM4_Update_Handler() __interrupt(25)
{
   // Clear interrupt
   TIM4_SR1 = TIM4_SR1 & ~0x01;

   // Increment Global Counter
   tickCount++;
}

void AWU_Handler() __interrupt(4)
{
   volatile uint8_t reg = AWU_CSR;
}

void deepSleep()
{
   //Activate AWU, Sleep for ~250ms

   // Set auto-wakeup delay
   AWU_TBR = 9;

   //250ms = 2^5 * APRdiv/Fls
   //250ms * Fls = 2^8 * APRdiv
   //(250ms * Fls) / 2^8 = APRdiv
   // Set Prescaler
   AWU_APR = 37-2;

   // Enable AWU
   AWU_CSR = 0x10;

   // Execute HALT instruction
   __asm__("HALT");
}

void configureInfraredLedPin()
{
   // IR Pin Output
   // Disable SWIM Mode
   CFG_GCR = CFG_GCR_SWIM_DIS;
   PORTA_DDR = PORTx_OUTPUT( IR_PIN ) | PORTA_DDR;
   PORTA_CR1 = PORTx_PUSH_PULL( IR_PIN ) | PORTA_CR1;
}

void configureLedPin()
{
   // LED Pin Output Push-Pull
   PORTB_DDR = PORTx_OUTPUT( LED_PIN ) | PORTB_DDR;
   PORTB_CR1 = PORTx_PUSH_PULL( LED_PIN ) | PORTB_CR1;
}

void configureSwitchPin()
{
   // SW1 Pin Input w/ Pull-up
   PORTB_CR1 = PORTx_PULLUP( SW1_PIN ) | PORTB_CR1;
}

void configureTIM2()
{
   // Configure timer to 38kHz ~11% duty cycle PWM

   TIM2_PSCR = 0; // f_master / 2^(TIM2_PSCR) = 2MHz / 1 = 2MHz

   uint16_t counterValue = 52;
   TIM2_ARRH = (uint8_t)(counterValue >> 8);
   TIM2_ARRL = (uint8_t)(counterValue);

   TIM2_CCER1 = 0;
   TIM2_CCMR1 = 0;

   // TIM2 Output Compare PWM Mode 1
   TIM2_CCMR1 = 0x60;

   // Output state enable, polarity high
   TIM2_CCER1 = 0x01;

   // TIM2 Output Idle (reset)
   TIM2_OISR = 0x00;

   // Pulse Value
   uint16_t pulseValue = 6;
   TIM2_CCR1H = (uint8_t)(pulseValue >> 8);
   TIM2_CCR1L = (uint8_t)(pulseValue);

   // Enable timer
   TIM2_CR1 = TIMx_CR1_CEN;

   // Main output enable
   TIM2_BKR = 0x80;
}

void configureTIM3()
{
   // Configure timer to any value. Will use
   // force active/inactive to enable IR LED

   TIM3_PSCR = 0; // f_master / 2^(TIM3_PSCR) = 2MHz / 1 = 2MHz

   uint16_t counterValue = 14224;
   TIM3_ARRH = (uint8_t)(counterValue >> 8);
   TIM3_ARRL = (uint8_t)(counterValue);

   TIM3_CCER1 = 0;
   TIM3_CCMR1 = 0;

   // TIM3 Output Compare Frozen
   TIM3_CCMR1 = 0x00;

   // Output state enable, polarity high
   TIM3_CCER1 = 0x01;

   // TIM3 Output Idle (reset)
   TIM3_OISR = 0x00;

   // Pulse Value
   uint16_t pulseValue = 14000;
   TIM3_CCR1H = (uint8_t)(pulseValue >> 8);
   TIM3_CCR1L = (uint8_t)(pulseValue);

   // Enable timer
   TIM3_CR1 = TIMx_CR1_CEN;

   // Main output enable
   TIM3_BKR = 0x80;
}

void configureTIM4()
{
   // Configure timer for 100us tick. Will use
   // this to create a more accurate 1ms delay.

   TIM4_PSCR = 4; // f_master / 2^(TIM4_PSCR) = 2MHz / 16 = 125kHz

   TIM4_ARR = 125; // 1/125kHz * 125 = 1ms

   TIM4_CR1 = TIMx_CR1_CEN;

   TIM4_IER = 0x01; // Enable Update interrupt

   // Enable interrupts
   __asm__("RIM");
}

void main(void)
{
   // Set the f_master to 16/8 MHz = 2MHz
   CLK_CKDIVR = 0x03;

   // Configure all pins except PA0
   // It is shared with SWIM and we need it
   // to reprogram the device. Use SW1 to allow
   // reprogramming.
   configureLedPin();
   configureSwitchPin();

   // Enable Master FCLK to TIM2, TIM3, and AWU
   CLK_PCKENR = CLK_EN_TIM2 | CLK_EN_TIM3 | CLK_EN_TIM4 | CLK_EN_AWU_BEEP;

   // Configure both timers for time keeping
   configureTIM2();
   configureTIM3();
   configureTIM4();

   // Check if switch is depressed and enter
   // infinite loop to allow reprogramming.
   if( ( PORTB_IDR & PORTx_VAL(SW1_PIN) ) == 0 )
   {
      while(1)
      {
         PORTB_ODR ^= PORTx_VAL(LED_PIN);
         delayTick(250);
      }
   }

   // Delay for a few seconds to give SWIM time just in case
   // of a bad flash before we reconfigure PA0 (SWIM)
   for( uint8_t seconds = 0; seconds < STARTUP_DELAY; seconds++ )
   {
      delayTick(1000);
      PORTB_ODR ^= PORTx_VAL(LED_PIN);
   }

   // Disable LED to conserve power
   PORTB_ODR = 0x00;

   // Configure IR LED Pin (SWIM will not function after this)
   // Use SW1 to enter an infinite loop to reprogram system
   configureInfraredLedPin();

   while( 1 )
   {
      // Generate IR LED pulse
      // 0 bit = 1ms on, 3 ms off
      // 1 bit = 3ms on, 1 ms off
      uint8_t irCommand = VIRTUALWALL_LIGHTHOUSE;
      volatile int8_t bitNum = 7;

      for( uint8_t pulseCount = 0; pulseCount < PULSE_COUNT; pulseCount++ )
      {
         // 0 = 1ms on, 3ms off
         // 1 = 3ms on, 1ms off
         for( bitNum = 7; bitNum >= 0; bitNum-- )
         {
            if( ( irCommand & ( 1 << bitNum ) ) != 0x00 )
            {
               // 3ms on, 1ms off
               TIM3_CCMR1 = TIMx_CCMR1_FORCE_ACTIVE;
               IR_CR = IR_CR_IR_EN;
               sleepTick( 3 );
               TIM3_CCMR1 = TIMx_CCMR1_FORCE_INACTIVE;
               IR_CR = 0;
               sleepTick( 1 );
            }
            else
            {
               // 1ms on, 3ms off
               TIM3_CCMR1 = TIMx_CCMR1_FORCE_ACTIVE;
               IR_CR = IR_CR_IR_EN;
               sleepTick( 1 );
               TIM3_CCMR1 = TIMx_CCMR1_FORCE_INACTIVE;
               IR_CR = 0;
               sleepTick( 3 );
            }
         }
         // After sending 8-bit pattern, one bit of silence
         sleepTick( 4 );
      }

      // Enter a power saving state. We only need to send the
      // command every so often so spend a lot of time in HALT
      // to conserve energy.
      deepSleep();
   }
}
